<?php

/*{{{ v.151005.001 (0.0.2)

	Sample config file for bitbucket hooks.

	Based on 'Automated git deployment' script by Jonathan Nicoal:
	http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/

	See README.md and CONFIG.php

	---
	Igor Lilliputten
	mailto: igor at lilliputten dot ru
	http://lilliputtem.ru/

}}}*/

/*{{{ Auxiliary variables, used only for constructing $CONFIG and $PROJECTS  */

$REPOSITORIES_PATH = '/var/www/git/';
$PROJECTS_PATH = '/var/www/html/';

/*}}}*/

// Base tool configuration:
$CONFIG = array(
	'bitbucketUsername' => 'ch8rt', // User name on bitbucket.org, *REQUIRED*
	'gitCommand' => '/usr/bin/git', // Git command, *REQUIRED*
	'repositoriesPath' => $REPOSITORIES_PATH, // Folder containing all repositories, *REQUIRED*
	'log' => true, // Enable logging, optional
	'logFile' => 'bitbucket.log', // Logging file name, optional
	// 'logClear' => true, // clear log each time, optional
	 'verbose' => false, // show debug info in log, optional
	// 'folderMode' => 0700, // creating folder mode, optional
);

// List of deploying projects:
$PROJECTS = array(
	// 'repo-name' => array( // The key is a bitbucket.org repository name
	// 	'projPath' => $PROJECTS_PATH.'deploy_path/', // Path to deploy project, *REQUIRED*
	// 	// 'postHookCmd' => 'your_command', // command to execute after deploy, optional
	// 	// 'branch' => 'master', // Deploying branch, optional
	// ),
	'bitbucket-deploy' => array(
		'projPath' 	=> $PROJECTS_PATH . 'deploy'
	),
	'atsb' => array(
		'projPath' 	=> $PROJECTS_PATH . 'asthingsshouldbe.com'
	),
	'ahum' => array(
		'projPath' 	=> $PROJECTS_PATH . 'asthingsshouldbe.com/d/ahum',
		'branch'	=> 'review'
	),
	'MusicMatters' 	=> array(
		'projPath' 	=> $PROJECTS_PATH . 'asthingsshouldbe.com/d/musicmatters',
		'branch'	=> 'review'
	),
	'clearandsimple' 	=> array(
		'projPath' 	=> $PROJECTS_PATH . 'clearandsimplehome.com'
	),
	'pathway2pro' 	=> array(
		'projPath' 	=> $PROJECTS_PATH . 'asthingsshouldbe.com/d/p2p'
	),
	'afHome' 	=> array(
		'projPath' 	=> $PROJECTS_PATH . 'asthingsshouldbe.com/d/af'
	),
	'rex-marketing' 	=> array(
		'projPath' 	=> $PROJECTS_PATH . 'asthingsshouldbe.com/d/rex'
	),
	'altGaming' 	=> array(
		'projPath' 	=> $PROJECTS_PATH . 'altgaming.uk'
	),
	'hebditch' 	=> array(
		'projPath' 	=> $PROJECTS_PATH . 'asthingsshouldbe.com/d/hebditch'
	),
	'Athlete Angel' 	=> array(
		'projPath' 	=> $PROJECTS_PATH . 'athleteangelhq.com'
	),
	'MF Racing Club' 	=> array(
		'projPath' 	=> $PROJECTS_PATH . 'asthingsshouldbe.com/d/mfrc'
	),
	'altlan' 	=> array(
		'projPath' 	=> $PROJECTS_PATH . 'altlan.co.uk'
	),
    'woti-api' 	=> array(
        'projPath' 	=> $PROJECTS_PATH . 'woti-api'
    ),
    'woti-web' 	=> array(
        'projPath' 	=> $PROJECTS_PATH . 'woti-web'
    )
	
);


